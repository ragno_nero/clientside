﻿using System;

namespace ClientSide
{
    class Program
    {

        static void Main(string[] args)
        {
            Communicator communicator = new Communicator();
            bool connection = communicator.CheckConnection();
            bool menu = true;
            if (connection)
            {
                while (menu)
                {
                    int input = EnterMenuChoice();
                    switch (input)
                    {
                        case 1:
                            Console.WriteLine("Баланс = " + communicator.GetData("balance"));
                            break;
                        case 2:
                            Console.WriteLine("Сума зароблених коштів за останню хвилину: " + communicator.GetData("profit"));
                            break;
                        case 3:
                            int count = communicator.GetCount();
                            Console.WriteLine($"Кількість вільних місць на парковці: {Setting.Capacity - count}, зайнято: {count} місць");
                            break;
                        case 4:
                            var transactions = communicator.GetTransactions(false);
                            if (transactions != null && transactions != String.Empty)
                            {
                                Console.WriteLine("Транзакції: ");
                                Console.WriteLine(transactions);
                            }
                            else Console.WriteLine("Транзакції відсутні!");
                            break;
                        case 5:
                            var allTransactions = communicator.GetTransactions(true);
                            if (allTransactions != null && allTransactions != String.Empty)
                            {
                                Console.WriteLine("Транзакції: ");
                                Console.WriteLine(allTransactions);
                            }
                            break;
                        case 6:
                            var transports = communicator.GetTransport();
                            if (transports != null && transports.Count > 0)
                            {
                                Console.WriteLine("Транспортні засоби: ");
                                foreach (var t in transports)
                                    Console.WriteLine("\t" + t.ToString());
                            }
                            else Console.WriteLine("Транспортні засоби відсутні!");
                            break;
                        case 7:
                            Transport tr = CreateTransport();
                            if (tr == null) break;
                            communicator.AddTransport(tr);
                            break;
                        case 8:
                            int removeId = EnterId();
                            if (removeId == -1) return;
                            communicator.RemoveTransport(removeId);
                            break;
                        case 9:
                            int refillId = EnterId();
                            float balance = EnterBalance();
                            if (refillId == -1 || balance < 0) break;
                            communicator.RefillBalance(refillId, balance);
                            break;
                        case 0:
                            menu = false;
                            break;
                        default:
                            Console.WriteLine("Такий пункт меню відсутній! Повторіть, будь ласка, ввід!");
                            break;
                    }
                    Console.ReadKey();
                    Console.Clear();
                }
            }
        }

        private static void Menu()
        {
            Console.Write("\tПрограма \'Симуляція парковки\':\n" +
                                 "1. Дізнатися баланс парковки\n" +
                                 "2. Сума зароблених коштів за останню хвилину\n" +
                                 "3. Дізнатися кількість вільних/зайнятих місць на парковці\n" +
                                 "4. Вивести на екран усі Транзакції Праковки за останню хвилину\n" +
                                 "5. Вивести усю історію Транзакцій\n" +
                                 "6. Вивести на екран список усіх Транспортних засобів\n" +
                                 "7. Створити/поставити Транспортний засіб на Парковку\n" +
                                 "8. Видалити/забрати Транспортний засіб з Парковки\n" +
                                 "9. Поповнити баланс конкретного Транспортного засобу\n" +
                                 "0. Вихід\n" +
                                 "\t Ваш вибір: ");
        }

        private static Transport CreateTransport()
        {
            int type = EnterType();
            if (type == -1) return null;
            float balance = EnterBalance();
            if (balance > 0)
            {
                Transport tr = new Transport(balance);
                switch (type)
                {
                    case 1:
                        tr.Type = "Car";
                        break;
                    case 2:
                        tr.Type = "Bus";
                        break;
                    case 3:
                        tr.Type = "Truck";
                        break;
                    case 4:
                        tr.Type = "Motorcycle";
                        break;
                }
                return tr;
            }
            return null;
        }

        private static int EnterId()
        {
            try
            {
                Console.Write("Введіть Id автомобіля: ");
                int id = Int32.Parse(Console.ReadLine());
                if (id < 0) throw new IndexOutOfRangeException("Exception: Значення знаходяться поза діапазоном допустимих значень");
                return id;
            }
            catch (IndexOutOfRangeException re)
            {
                Console.WriteLine(re.Message);
                return -1;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return -1;
            }
        }

        private static int EnterType()
        {
            try
            {
                Console.Write("Тип транспорту (1. Автомобіль, 2. Автобус, 3. Вантажний, 4. Мотоцикл): ");
                int type = Int32.Parse(Console.ReadLine());
                if (type < 1 || type > 4) throw new IndexOutOfRangeException("Значення знаходяться поза діапазоном допустимих значень");
                return type;
            }
            catch (IndexOutOfRangeException re)
            {
                Console.WriteLine(re.Message);
                return -1;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return -1;
            }
        }

        private static float EnterBalance()
        {
            try
            {
                Console.Write("Введіть баланс автомобіля: ");
                float balance = float.Parse(Console.ReadLine());
                if (balance <= 0) throw new IndexOutOfRangeException("Значення знаходяться поза діапазоном допустимих значень");
                return balance;
            }
            catch (IndexOutOfRangeException re)
            {
                Console.WriteLine(re.Message);
                return -1f;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1f;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return -1f;
            }
        }

        private static int EnterMenuChoice()
        {
            try
            {
                Menu();
                int choice = Int32.Parse(Console.ReadLine());
                if (choice < 0 || choice > 10) throw new IndexOutOfRangeException("Значення знаходяться поза діапазоном допустимих значень");
                return choice;
            }
            catch (IndexOutOfRangeException re)
            {
                Console.WriteLine(re.Message);
                return -1;
            }
            catch (ArgumentException)
            {
                Console.WriteLine("Exception: Введено некоректне значення");
                return -1;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
                return -1;
            }
        }
    }
}
