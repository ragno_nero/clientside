﻿namespace ClientSide
{
    struct Rate
    {
        public float Car { get; set; }
        public float Truck { get; set; }
        public float Bus { get; set; }
        public float Motocycle { get; set; }
    }
}
