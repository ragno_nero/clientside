﻿namespace ClientSide
{
    public class Transport
    {
        public int Id { get; set; }
        public float Balance { get; set; }
        public string Type { get; set; }

        public Transport(float balance)
        {
            Balance = balance;
        }

        public override string ToString()
        {
            return $"Id {Id}, Тип: {Type}, Баланс: {Balance}";
        }
    }
}
