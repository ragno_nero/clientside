﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ClientSide
{
    class Communicator
    {
        public static string Url { get; set; }
        public const string Transport = "transport/";
        public const string Transactions = "transaction/";
        public const string Setting = "setting/";
        public const string Parking = "parking/";

        static Communicator()
        {
            Url = @"https://localhost:44390/api/";
        }

        public Communicator()
        { }

        public double GetData(string path)
        {
            using (HttpClient Client = new HttpClient())
            {
                var result = Client.GetAsync(Url + Parking + path).GetAwaiter().GetResult();
                result.EnsureSuccessStatusCode();
                return double.Parse(result.Content.ReadAsStringAsync().GetAwaiter().GetResult().ToString());
            }
        }

        public int GetCount(string path = "count")
        {
            using (HttpClient Client = new HttpClient())
            {
                var result = Client.GetAsync(Url + Parking + path).GetAwaiter().GetResult();
                result.EnsureSuccessStatusCode();
                return int.Parse(result.Content.ReadAsStringAsync().GetAwaiter().GetResult().ToString());
            }
        }

        public string GetTransactions(bool all)
        {
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    int flag = (all == true) ? 1 : 0;

                    var result = Client.GetAsync(Url + Parking + Transactions + flag).GetAwaiter().GetResult();
                    result.EnsureSuccessStatusCode();
                    return result.Content.ReadAsStringAsync().GetAwaiter().GetResult();

                }
            }
            catch
            {
                Console.WriteLine("Файл з транзакціями вісутній на сервері");
                return null;
            }
        }

        public List<Transport> GetTransport()
        {
            using (HttpClient Client = new HttpClient())
            {
                var result = Client.GetAsync(Url + Parking + Transport).GetAwaiter().GetResult();
                result.EnsureSuccessStatusCode();
                return JsonConvert.DeserializeObject<List<Transport>>(result.Content.ReadAsStringAsync().GetAwaiter().GetResult());
            }
        }

        public void RemoveTransport(int id)
        {
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    var result = Client.DeleteAsync(Url + Parking + Transport + id).GetAwaiter().GetResult();
                    result.EnsureSuccessStatusCode();
                    Console.WriteLine("Автомобіль видалено!");
                }
            }
            catch
            {
                Console.WriteLine("Не вдалося видалити автомобіль!");
            }
        }

        public void AddTransport(Transport transport)
        {

            var stringContent = new StringContent(JsonConvert.SerializeObject(transport), Encoding.UTF8, "application/json");
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    var result = Client.PostAsync(Url + Parking + Transport, stringContent).GetAwaiter().GetResult();
                    result.EnsureSuccessStatusCode();
                    Console.WriteLine($"Додано автомобіль: {transport.ToString()}");
                }
            }
            catch
            {
                Console.WriteLine("Не вдалося додати автомобіль");
            }
        }

        public void RefillBalance(int id, float fee)
        {
            var stringContent = new StringContent(JsonConvert.SerializeObject(fee), Encoding.UTF8, "application/json");
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    var result = Client.PutAsync(Url + Parking + Transport + id.ToString(), stringContent).GetAwaiter().GetResult();
                    result.EnsureSuccessStatusCode();
                    Console.WriteLine($"Баланс автомобіля з Id {id} пововнено на {fee}!");
                }
            }
            catch
            {
                Console.WriteLine("Не вдалося вдалося поповнити баланс автомобіля!");
            }
        }

        public static NonStaticSetting GetSetting()
        {
            using (HttpClient client = new HttpClient())
            {
                string x = client.GetStringAsync(Url + Setting).GetAwaiter().GetResult();
                NonStaticSetting st = JsonConvert.DeserializeObject<NonStaticSetting>(x);
                return st;
            }
        }

        public bool CheckConnection()
        {
            int timeout = 10000;
            try
            {
                using (HttpClient Client = new HttpClient())
                {
                    var response = Client.GetAsync(Url).Wait(timeout);
                    return response;
                }
            }
            catch
            {
                Console.WriteLine("Сервер недоступний! Спробуйте під'єднатися пізніше!");
                return false;
            }
        }
    }
}
