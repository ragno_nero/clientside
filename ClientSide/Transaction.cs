﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientSide
{
    public class Transaction
    {
        public int Id { get; set; }
        public DateTime Time { get; set; }
        public int CarId { get; set; }
        public float MoneyAmount { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}, Time: {Time.ToString("HH:mm:ss")}, CarId: {CarId}, MoneyAmount: {MoneyAmount}";
        }
    }
}
