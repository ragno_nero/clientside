﻿using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace ClientSide
{
    static class Setting
    {
        public static double Balance { get; private set; }
        public static int Capacity { get; private set; }
        public static int PayTime { get; private set; }
        public static Rate Tariff { get; private set; }
        public static float Fine { get; private set; }

        static Setting()
        {
            GetSettings();
        }

        public static void GetSettings()
        {
            NonStaticSetting setting = Communicator.GetSetting();
            Balance = setting.Balance;
            Capacity = setting.Capacity;
            PayTime = setting.PayTime;
            Tariff = setting.Tariff;
            Fine = setting.Fine;
        }
    }

    class NonStaticSetting
    {
        [JsonProperty("balance")]
        public double Balance { get; private set; }
        [JsonProperty("capacity")]
        public int Capacity { get; private set; }
        [JsonProperty("payTime")]
        public int PayTime { get; private set; }
        [JsonProperty("tariff")]
        public Rate Tariff { get; private set; }
        [JsonProperty("fine")]
        public float Fine { get; private set; }
    }
}
